import 'package:flutter/material.dart';
import 'package:mealapp/model/meal.dart';

import '../widgets/main_drawer.dart';
import './category_screen.dart';
import './favourites_screen.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favoriteMeals;

  TabsScreen(this.favoriteMeals);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  List<Map<String, dynamic>> _pages;

  int _selectTabIndex = 0;

  @override
  void initState() {
    _pages = [
      {'page': CategoryScreen(), 'title': 'Categories'},
      {'page': FavouritesScreen(widget.favoriteMeals), 'title': 'Favorites'}
    ];
    super.initState();
  }

  void _selectBottomTab(int index) {
    setState(() {
      _selectTabIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectTabIndex]['title']),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectTabIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectBottomTab,
        backgroundColor: Theme
            .of(context)
            .primaryColor,
        unselectedItemColor: Colors.black,
        selectedItemColor: Theme
            .of(context)
            .accentColor,
        currentIndex: _selectTabIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.category), title: Text('Categories'),),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite), title: Text('Favorites'),),
        ],),
    );
  }
}
