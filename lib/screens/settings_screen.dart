import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class SettingsScreen extends StatefulWidget {
  static const routeName = '/settings';

  final Function saveFilters;
  final Map<String, bool> currentFilters;

  SettingsScreen(this.currentFilters, this.saveFilters);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  var _glutenFree = false;
  var _vegetarian = false;
  var _vegan = false;
  var _lactoseFree = false;

  @override
  initState(){
    _glutenFree = widget.currentFilters['gluten'];
    _vegetarian = widget.currentFilters['vegetarian'];
    _vegan = widget.currentFilters['vegan'];
    _lactoseFree = widget.currentFilters['lactose'];
    super.initState();
  }

  Widget _buildListTile(
      String title, String description, bool value, Function updateValue) {
    return SwitchListTile(
      title: Text(
        title,
      ),
      subtitle: Text(description),
      value: value,
      onChanged: updateValue,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.save), onPressed: () {
            final selectedFilters = {
              'gluten': _glutenFree,
              'lactose': _lactoseFree,
              'vegan': _vegan,
              'vegetarian': _vegetarian,
            };
            widget.saveFilters(selectedFilters);
          },),
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(20),
            child: Text('Filter your meal!', style: Theme.of(context).textTheme.headline6,),
          ),
          Expanded(
            child: ListView(
              children: <Widget>[
                _buildListTile(
                  'Gluten-Free',
                  'This shows gluten free meals',
                  _glutenFree,
                  (switchValue) {
                    setState(() {
                      _glutenFree = switchValue;
                    });
                  },
                ),
                _buildListTile(
                  'Lactose-Free',
                  'This shows lactose free meals',
                  _lactoseFree,
                      (switchValue) {
                    setState(() {
                      _lactoseFree = switchValue;
                    });
                  },
                ),
                _buildListTile(
                  'Vegetarian',
                  'This shows vegetarian meals',
                  _vegetarian,
                      (switchValue) {
                    setState(() {
                      _vegetarian = switchValue;
                    });
                  },
                ),
                _buildListTile(
                  'Vegan',
                  'This shows vegan free meals',
                  _vegan,
                      (switchValue) {
                    setState(() {
                      _vegan = switchValue;
                    });
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
