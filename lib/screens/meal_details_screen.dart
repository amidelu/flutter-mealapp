import 'package:flutter/material.dart';

import '../dummy_data.dart';

class MealDetailScreen extends StatelessWidget {
  static const routeName = '/meal-detail';

  final Function toggleFavorite;
  final Function isFavorite;

  MealDetailScreen(this.toggleFavorite, this.isFavorite);

  @override
  Widget build(BuildContext context) {
    final mealId = ModalRoute.of(context).settings.arguments as String;
    final selectedMeal = DUMMY_MEALS.firstWhere((meal) => meal.id == mealId);

    Widget buildWidgetTitle(BuildContext context, String titleText) {
      return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        child: Text(
          titleText,
          style: Theme.of(context).textTheme.headline6,
        ),
      );
    }

    Widget buildWidgetListStyle(Widget child) {
      return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(10),
          ),
          height: 150,
          width: 300,
          margin: const EdgeInsets.all(10),
          padding: const EdgeInsets.all(10),
        child: child,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(selectedMeal.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                selectedMeal.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            buildWidgetTitle(context, 'Ingredients'),
            buildWidgetListStyle(
            ListView.builder(
              itemBuilder: (ctx, index) => Card(
                color: Theme.of(context).accentColor,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                  child: Text(selectedMeal.ingredients[index]),
                ),
              ),
              itemCount: selectedMeal.ingredients.length,
            )),
            buildWidgetTitle(context, 'Steps'),
            buildWidgetListStyle(ListView.builder(itemBuilder: (ctx, index) => ListTile(
              leading: CircleAvatar(
                child: Text('# ${(index +1)}'),
              ),
              title: Text(selectedMeal.steps[index]),
            ),
              itemCount: selectedMeal.steps.length,
            ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(isFavorite(mealId) ? Icons.star : Icons.star_border),
        onPressed: () => toggleFavorite(mealId),
      ),
    );
  }
}
